var express = require('express');
var router = express.Router();

var arr = [
  {
    id: 1,
    name: 'Artem'
  },
  {
    id: 2,
    name: 'Stas'
  }
];

/* GET users listing. */
router.get('/', function (req, res, next) {

  res.send(arr);
});

router.get('/:id', function (req, res, next) {
  res.send(arr[req.params.id - 1]);
});

router.post('/create', function (req, res, next) {
  let user = req.body.user;
  arr.push(user);
  res.send(arr);
});

router.put('/:id', function (req, res, next) {
  let user_id = req.params.id;
  var item = arr.find(item => item.id == user_id);
  let newData = { id: 1, name: 'keks' };
  item.id = newData.id;
  item.name = newData.name;
  res.send(arr);
});

router.delete('/:id', function (req, res, next) {
  let user_id = req.params.id;
  var item = arr.find(item => item.id == user_id);
  arr.splice(item, 1);
  res.send(arr);
});

module.exports = router;
